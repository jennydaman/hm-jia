{ lib, pkgs, inputs, ... }:

{
  programs.foot = {
    enable = true;
    settings = {
      main.font = "MesloLGS NF:size=12";
      scrollback.lines = 10000;
      colors = {
        alpha = 0.8;
        background = "2a2a2a";
        foreground = "b3b9c5";

        regular1 = "f2777a";  # red
        regular2 = "92d192";  # green
        regular3 = "ffd479";  # yellow
        regular4 = "6ab0f3";  # blue
        regular5 = "e1a6f2";  # magenta
        regular6 = "76d4d6";  # cyan
        regular7 = "ffffff";  # white
      };
    };
  };
}
