# Defines a hyprlock configuration where the background is a blurred screenshot.
# It is revealing, but looks good.

{ config, lib, pkgs, ... }:

let
  screenshotPath = "${config.xdg.cacheHome}/screenshot.png";
in {
  services.hypridle = let
    _blurredhyprlockPkg = (pkgs.writeShellScriptBin "blurredhyprlock" ''
      set -e
      ${pkgs.grimblast}/bin/grimblast save screen ${screenshotPath}
      exec ${pkgs.hyprlock}/bin/hyprlock
    '');
    blurredhyprlock = "${_blurredhyprlockPkg}/bin/blurredhyprlock";
  in {
    enable = true;
    settings = {
      general = {
        lock_cmd = "pidof hyprlock || ${blurredhyprlock}";
        unlock_cmd = "pkill -USR1 hyprlock";
        before_sleep_cmd = "loginctl lock-session";  # lock before suspend.
        after_sleep_cmd = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";  # to avoid having to press a key twice to turn on the display.
      };
      # note: listener is configured in ../my_laptop.nix
    };
  };

  programs.hyprlock = {
    enable = true;
    settings = let
      fontFamily = "Cantarell 10";
    in {
      background = [
        {
          monitor = "";
          path = "${config.xdg.cacheHome}/screenshot.png";
          blur_size = 4;
          blur_passes = 3;
          noise = 0.0117;
          contrast = 1.3;
          brightness = 0.8;
          vibrancy = 0.21;
          vibtrancy_darkness = 0.0;
        }
      ];
      input-field = [
        {
          monitor = "";
          size = "50, 50";
          placeholder_text = "";
          fail_text = "";
          outline_thickness = 10;
          fade_on_empty = true;
          hide_input = true;
          position = "60, 60";
          halign = "left";
          valign = "bottom";
        }
      ];
      label = [
        {
          monitor = "";
          text = "cmd[update:1000] echo \"<b><big>$(date +\"%H:%M:%S\")</big></b>\"";
          color = "";
          font_size = 20;
          font_family = "${fontFamily}";
          position = "150, 60";
          halign = "left";
          valign = "bottom";
        }
        {
          monitor = "";
          text = "${config.home.username}";
          font_size = 16;
          font_family = "${fontFamily}";
          position = "150, 100";
          halign = "left";
          valign = "bottom";
        }
      ];
    };
  };
}
