{ lib, pkgs, inputs, ... }:

{
  home.file.".config/anyrun/config.ron".text = ''
    Config(

      // The horizontal position, adjusted so that Relative(0.5) always centers the runner
      x: Fraction(0.5),

      // The vertical position, works the same as `x`
      y: Fraction(0.25),

      width: Absolute(800),
      height: Absolute(0),

      vertical_offset: Absolute(-100), // How much the runner is shifted vertically
      hide_icons: false,
      ignore_exclusive_zones: true, // ignore exclusive zones, f.e. Waybar
      layer: Overlay, // GTK Layer: Bottom, Top, Background, Overlay
      hide_plugin_info: false,


      // Close window when a click outside the main box is received
      close_on_click: true,

      // Show search results immediately when Anyrun starts
      show_results_immediately: false,


      // Limit amount of entries shown in total
      max_entries: Some(8),

      plugins: [
        "libapplications.so",
        "libsymbols.so",
        // "libshell.so",
        // "libtranslate.so",
      ],
    )
  '';

  home.file.".config/anyrun/style.css".text = ''
    /* https://github.com/Kirottu/anyrun#styling */

    /* copied from: https://github.com/abenz1267/anyrun/blob/master/style.css
     */

    #window {
        animation: fadeIn 250ms forwards;
    }

    @keyframes fadeIn {
        from {
            background: rgba(22, 22, 29, 0);
        }
        to {
            background: rgba(22, 22, 29, 0.6);
        }
    }

    entry {
        background: rgba(20, 20, 20, 0.8);
        color: #EEE;
        caret-color: #dcd7ba;
        padding: 10px 8px;
        border-radius: 10px 10px 0 0;
    }

    entry:focus,
    entry:hover,
    entry:active {
        outline: none;
        outline-width: 0px;
        box-shadow: none;
        border-bottom: none;
        border: none;
    }

    list#main {
        background: rgba(30, 30, 30, 0.8);
        border-radius: 0 0 10px 10px;
    }

    list#main > :first-child {
        margin-left: 4px;
    }

    image,
    image#plugin {
        opacity: 1;
    }

    #plugin {
        background-color: transparent;
        color: #dcd7ba;
    }

    label#plugin {
        opacity: 0.25;
    }

    #match {
        border-right: 4px solid transparent;
        padding: 5px 0;
    }

    #match-desc {
        opacity: 0.5;
        font-size: 12px;
    }

    #match:selected {
        border-right: 4px solid #BBB;
        color: #FFF;
        background: #222;
    }
  '';
}
