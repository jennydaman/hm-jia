{ config, lib, pkgs, ... }:

{
  wayland.windowManager.hyprland.settings = {
    general = {
      # which layout to use. [dwindle/master]
      layout = "dwindle";

      # enables resizing windows by clicking and dragging on borders and gaps
      resize_on_border = true;
    };

    dwindle = {
      pseudotile = true;
      preserve_split = true;  # you probably want this
    };

    "$mainMod" = "SUPER";

    bind = [
      # overview
      # "$mainMod, W, hyprexpo:expo, toggle"

      # Lock screen
      "CTRL ALT, DELETE, exec, loginctl lock-session"

      # app shortcuts
      "$mainMod, T, exec, foot"
      "$mainMod, space, exec, pkill anyrun || anyrun"
      "$mainMod, E, exec, nautilus --new-window"

      # screenshots
      "          , Print, exec, grimblast copy screen"
      "CTRL      , Print, exec, grimblast copy area"
      "     SHIFT, Print, exec, sh -c 'grimblast save screen ~/Pictures/Screenshots/screenshot_$(date +%Y%m%d_%H_%M_%S).png'"
      "CTRL SHIFT, Print, exec, sh -c 'grimblast save area   ~/Pictures/Screenshots/screenshot_$(date +%Y%m%d_%H_%M_%S).png'"

      # dismiss all notifications
      "$mainMod SHIFT, P, exec, makoctl dismiss --all"

      # show clipboard history
      "$mainMod, V, exec, cliphist list | wofi -d -s ~/.dotfiles/wofi/clipboard_history.css | cliphist decode | wl-copy"

      "$mainMod, Q, killactive,"
      "$mainMod, F, togglefloating,"
      "$mainMod, P, pseudo"       # dwindle
      "$mainMod, S, togglesplit"  # dwindle

      # Move focus with mainMod + VIM directional keys
      "$mainMod, H, movefocus, l"
      "$mainMod, J, movefocus, r"
      "$mainMod, K, movefocus, u"
      "$mainMod, L, movefocus, d"

      # Resize focused window
      "$mainMod ALT, L, resizeactive,  100 0"
      "$mainMod ALT, H, resizeactive, -100 0"
      "$mainMod ALT, K, resizeactive,  0  -100"
      "$mainMod ALT, J, resizeactive,  0   100"

      # Move window
      "$mainMod SHIFT, H, movewindow, l"
      "$mainMod SHIFT, L, movewindow, r"
      "$mainMod SHIFT, K, movewindow, u"
      "$mainMod SHIFT, J, movewindow, d"

      # Switch workspaces with mainMod + [0-9]
      "$mainMod, 1, workspace, 1"
      "$mainMod, 2, workspace, 2"
      "$mainMod, 3, workspace, 3"
      "$mainMod, 4, workspace, 4"
      "$mainMod, 5, workspace, 5"
      "$mainMod, 6, workspace, 6"
      "$mainMod, 7, workspace, 7"
      "$mainMod, 8, workspace, 8"
      "$mainMod, 9, workspace, 9"
      "$mainMod, 0, workspace, 10"

      # Move active window to a workspace with mainMod + SHIFT + [0-9]
      "$mainMod SHIFT, 1, movetoworkspace, 1"
      "$mainMod SHIFT, 2, movetoworkspace, 2"
      "$mainMod SHIFT, 3, movetoworkspace, 3"
      "$mainMod SHIFT, 4, movetoworkspace, 4"
      "$mainMod SHIFT, 5, movetoworkspace, 5"
      "$mainMod SHIFT, 6, movetoworkspace, 6"
      "$mainMod SHIFT, 7, movetoworkspace, 7"
      "$mainMod SHIFT, 8, movetoworkspace, 8"
      "$mainMod SHIFT, 9, movetoworkspace, 9"
      "$mainMod SHIFT, 0, movetoworkspace, 10"

      # Scroll through existing workspaces with mainMod + scroll
      "$mainMod, mouse_down, workspace, e+1"
      "$mainMod, mouse_up, workspace, e-1"

      # Full screen (monocle view)
      "$mainMod, M, fullscreen, 0"
      # Fake full screen
      "$mainMod SHIFT, M, fullscreenstate, -1, 3"
    ];

    binde = [
      ", XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+"
      ", XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
      ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle"
      ", XF86AudioMicMute, exec, wpctl set-mute @DEFAULT_AUDIO_SOURCE@ toggle"
    ];

    # Move/resize windows with mainMod + LMB/RMB and dragging
    bindm = [
      "$mainMod, mouse:272, movewindow"
      "$mainMod, mouse:273, resizewindow"
    ];

    # Laptop touchpad gestures
    gestures = {
      workspace_swipe = true;
      workspace_swipe_fingers = 3;
    };
  };
}
