{ config, lib, pkgs, ... }:

{
  services.cliphist.enable = true;

  services.mako = {
    enable = true;
    backgroundColor = "#222222CC";
    borderSize = 2;
    borderColor = "#333333";
  };
}
