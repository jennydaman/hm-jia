{ config, lib, pkgs, ... }:

{
  programs.waybar = {
    enable = true;
    style = ./waybar.css;
    settings.mainBar = {
      layer = "top";
      height = 30;
      spacing = 4;  # gaps between modules
      modules-left = [ "hyprland/workspaces" "hyprland/window"];
      modules-center = [ "clock" ];
      # note: modules-right is set in my_laptop.nix and my_desktop.nix
      tray = {
        spacing = 10;
      };
      clock = {
        tooltip-format = "<tt><small>{calendar}</small></tt>";
        format = "{:%c}";
        format-alt = "{:%T}";
        locale = "en_DK.UTF-8";
        interval = 1;
      };
      cpu = {
        format = "{usage}% ";
        tooltip = false;
      };
      memory = {
        format = "{}% ";
      };
      temperature = {
        format = "{temperatureC}°C {icon}";
        format-icons = ["" "" ""];
        critical-threshold = 80;
      };
      backlight = {
        format = "{percent}% {icon}";
        format-icons = ["" "" "" "" "" "" "" "" ""];
      };
      idle_inhibitor = {
        format = "";
      };
      battery = {
        states = {
          # good = 95;
          warning = 30;
          critical = 15;
        };
        format = "{capacity}% {icon}";
        format-charging = "{capacity}% ";
        format-plugged = "{capacity}% ";
        format-alt = "{time} {icon}";
        format-icons = ["" "" "" "" ""];
      };
      power-profiles-daemon = {
        format-icons = {
          default = "D";
          performance = "<U+F0E7>";
          balanced = "<U+F24E>";
          power-saver = "<U+F06C>";
        };
      };
      network = {
        format-wifi = "{essid} ({signalStrength}%) ";
        format-ethernet = "{ipaddr}/{cidr} ";
        tooltip-format = "{ifname} via {gwaddr} ";
        format-linked = "{ifname} (No IP) ";
        format-disconnected = "isconnected ⚠";
        format-alt = "{ifname}: {ipaddr}/{cidr}";
      };
      pulseaudio = {
        format = "{volume}% {icon} {format_source}";
        format-bluetooth = "{volume}% {icon} {format_source}";
        format-bluetooth-muted = " {icon} {format_source}";
        format-muted = " {format_source}";
        format-source = "{volume}% ";
        format-source-muted = "";
        format-icons = {
          headphone = "";
          hands-free = "";
          headset = "";
          phone = "";
          portable = "";
          car = "";
          default = ["" "" ""];
        };
        on-click = "pavucontrol";
      };
    };

    # start waybar when Hyprland starts
    # TODO bug: waybar does not restart if user logs out, logs in, and opens Hyprland a second time.
    # Either need to reboot or run `systemctl --user restart waybar`
    systemd = {
      enable = true;
      target = "hyprland-session.target";
    };
  };
}
