{ config, lib, pkgs, inputs, ... }:

{
  xdg.dataFile."icons/rose-pine-hyprcursor".source = "${inputs.rose-pine-hyprcursor.packages.${pkgs.system}.default}/share/icons/rose-pine-hyprcursor";
  wayland.windowManager.hyprland.settings = {
    env = [
      "HYPRCURSOR_THEME,rose-pine-hyprcursor"
      "HYPRCURSOR_SIZE,24"
    ];
    # blur more stuff!
    layerrule = [
      "blur,anyrun"
      "blur,wofi"
      "blur,notifications"
    ];

    # when attached to an HDMI monitor on the left...
    # monitor = [
    #   "eDP-1, preferred, auto, 1.25"
    #   "HDMI-A-1, preferred, auto-left, 1"
    # ];

    general = {
      gaps_in = 5;
      gaps_out = 20;
      border_size = 2;
      "col.active_border" = "rgba(33ccffee) rgba(00ff99ee) 45deg";
      "col.inactive_border" = "rgba(595959aa)";
      resize_on_border = true;
    };

    decoration = {
      rounding = 10;
    };

    animations = {
      enabled = true;
      # https://easings.net/#easeOutExpo
      bezier = "easeOutExpo, 0.16, 1, 0.3, 1";
      animation = [
        "windows, 1, 4, easeOutExpo"
        "windowsOut, 1, 4, default, popin 80%"
        "border, 1, 10, default"
        "borderangle, 1, 8, default"
        "fade, 1, 7, default"
        "workspaces, 1, 6, default"
      ];
    };
    plugin.hyprexpo = {
      gap_size = 10;
      bg_col = "rgb(111111)";
      workspace_method = "center current";  # [center/first] [workspace] e.g. first 1 or center m+1
      enable_gesture = true;   # laptop touchpad
      gesture_fingers = 3 ;    # 3 or 4
      gesture_distance = 300;  # how far is the "max"
      gesture_positive = false; # positive = swipe down. Negative = swipe up.
    };
  };
}
