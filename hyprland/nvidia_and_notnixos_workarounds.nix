
{ config, lib, pkgs, ... }:

{
  # prefer to use the Hyprland binary installed by OS package manager instead of from nixpkgs.
  # see https://github.com/hyprwm/Hyprland/issues/6967#issuecomment-2303084207
  programs.zsh.shellAliases."Hyprland" = "/usr/bin/Hyprland";

  # IMPORTANT: should also install at the system-level: xdg-desktop-portal-hyprland grim slurp

  wayland.windowManager.hyprland.settings = {
    # See https://wiki.hyprland.org/Nvidia/
    env = [
      "LIBVA_DRIVER_NAME,nvidia"
      "XDG_SESSION_TYPE,wayland"
      "GBM_BACKEND,nvidia-drm"
      "__GLX_VENDOR_LIBRARY_NAME,nvidia"
    ];
    cursor.no_hardware_cursors = true;

    # Not needed on NixOS, mako is started when needed by dbus.
    # On Arch Linux, this doesn't seem to happen.
    exec-once = [
      "${pkgs.mako}/bin/mako"
    ];
  };
}
