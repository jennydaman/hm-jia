{ config, lib, pkgs, inputs, ... }:

{
  wayland.windowManager.hyprland.enable = true;

  # wayland.windowManager.hyprland.plugins = with pkgs.hyprlandPlugins; [
  #   hyprexpo
  # ];

  wayland.windowManager.hyprland.settings.env = [
    # use Wayland for electron and chromium apps
    # https://nixos.wiki/wiki/Wayland#Electron_and_Chromium
    # https://wiki.hyprland.org/Nix/#nixos-module
    "NIXOS_OZONE_WL,1"
  ];

  home.packages = with pkgs; [
    # Wayland desktop
    waybar
    anyrun
    wofi
    grimblast  # screenshots
    libnotify
    cliphist
    wl-clipboard  # TODO: can I use wl-clipboard-rs instead?

    hyprpicker
  ];
}
