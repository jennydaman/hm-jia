{ config, lib, pkgs, ... }:

{
  wayland.windowManager.hyprland.settings.input = {
    kb_layout = "us";
    numlock_by_default = true;

    follow_mouse = 1;

    touchpad = {
      natural_scroll = true;
      tap-to-click = true;
    };

    sensitivity = 0;
    repeat_delay = 300;
    kb_options = "compose:ralt";
  };
}
