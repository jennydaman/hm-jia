{ config, lib, pkgs, ... }:

{
  programs.git = {
    enable = true;

    userName = "Jennings Zhang";
    userEmail = "dev@sl.jennin.xyz";

    delta.enable = true;

    aliases = {
      lg = "log --color --graph --date=local --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ad) %C(bold blue)<%an>%Creset' --abbrev-commit";
    };

    extraConfig = {
      push.autoSetupRemote = true;
      pull.rebase = false;
      color.ui = true;
    };
  };
}
