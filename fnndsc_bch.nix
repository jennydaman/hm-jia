# special settings to use for where I work: FNNDSC @ Boston Children's Hospital
{ config, lib, pkgs, inputs, ... }:

{
  # Home Manager can also manage your environment variables through
  # 'home.sessionVariables'. If you don't want to manage your shell through Home
  # Manager then you have to manually source 'hm-session-vars.sh' located at
  # either
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/jenni/etc/profile.d/hm-session-vars.sh
  #
  home.sessionVariables = {
    # Manually adding ~/.nix-profile/bin to PATH is necessary when using nix-portable.
    # ref: https://github.com/DavHau/nix-portable/issues/66#issuecomment-2067802826
    #
    # /neuro/labs/grantlab/users/jennings.zhang/nix-portable-bin is where nix-portable is installed.
    # Adding /neuro/arch/Linux64/bin to PATH is a FNNDSC specific thing.
    PATH = lib.mkForce "/neuro/labs/grantlab/users/jennings.zhang/nix-portable-bin:$HOME/.nix-profile/bin:/neuro/arch/Linux64/bin:$PATH";
  };

  programs.zsh.initExtra = ''
    export MAMBA_ROOT_PREFIX=/neuro/labs/grantlab/research/Jennings/opt/micromamba
    if [ -e "$MAMBA_ROOT_PREFIX" ]; then
      eval "$(micromamba shell hook --shell zsh --root-prefix "$MAMBA_ROOT_PREFIX")"
    fi

    if [ -e "$HOME/.rye" ]; then
      source "$HOME/.rye/env"
    fi
  '';

  home.packages = [
    (pkgs.writeShellScriptBin "fnndsc-home-manager" ''
      cd /neuro/labs/grantlab/users/jennings.zhang/.config/home-manager
      exec home-manager "$@" --impure --flake .#restricted
    '')
  ];

  programs.git = {
    userName = "Jennings Zhang";
    userEmail = lib.mkForce "Jennings.Zhang@childrens.harvard.edu";
    extraConfig.safe.directory = [ "/neuro/labs/grantlab/research/*" ];
  };
}
