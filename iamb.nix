{ config, lib, pkgs, ... }:

{
  home.packages = with pkgs; [
    iamb
  ];
  home.file.".config/iamb/config.toml".text = ''
    [settings.image_preview.protocol]
    type = "sixel"

    [settings.notifications]
    enabled = true
    show_message = true
    via = "desktop"

    [profiles.user]
    user_id = "@jennydaman:fedora.im"
  '';
}
