{ config, lib, pkgs, ... }:

{
  # must set cursor theme for virt-manager to work.
  # https://discourse.nixos.org/t/virt-manager-cannot-create-vm/38894/6
  # home.pointerCursor = {
  #   gtk.enable = true;
  #   package = pkgs.vanilla-dmz;
  #   name = "Vanilla-DMZ";
  # };

  # ref: https://github.com/nix-community/home-manager/blob/b00d0e4fe9cba0047f54e77418ddda5f17e6ef2c/modules/misc/gtk.nix#L112
  # gtk = {
  #   enable = true;
  #   # note: setting an icon theme is necessary to fix an odd bug in virt-manager.
  #   # without an icon theme, virt-manager will be unable to create VMs.
  #   # iconTheme = {
  #   #   package = pkgs.papirus-icon-theme;
  #   #   name = "Papirus-Dark";
  #   # };
  # };

  # It seems like no matter what I do, I can't get a dark theme to work, probably because of qt6 changes.
  # ref: https://github.com/nix-community/home-manager/blob/b00d0e4fe9cba0047f54e77418ddda5f17e6ef2c/modules/misc/qt.nix#L55
  # qt = {
  #   enable = true;

  #   # platformTheme = "gnome";
  #   # style.name = "adwaita-dark";
  
  #   style.name = "kvantum";
  #   # Note: If using kvantum, follow this guide to use kvantum theme in flatpak apps.
  #   # https://redlib.jennin.xyz/r/linux/comments/psit31/comment/hdpyvfb/?context=3
  #   #     flatpak install flathub org.kde.KStyle.Kvantum
  #   #     sudo flatpak override --env=QT_STYLE_OVERRIDE=kvantum --filesystem=xdg-config/Kvantum:ro
  #   #
  #   # These effects can be undone by running
  #   #
  #   #     sudo flatpak override --reset
  # };
}
