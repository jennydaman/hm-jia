{
  description = "Home Manager configuration of Jennings";

  inputs = {
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixgl = {
      # url = "github:nix-community/nixGL";
      inputs.nixpkgs.follows = "nixpkgs";
      # needs fix from PR https://github.com/nix-community/nixGL/pull/187
      url = "github:phirsch/nixGL/fix-versionMatch";
    };
    dovemail.url = "gitlab:jennydaman/dovemail";
    chrs.url = "github:FNNDSC/chrs";
    rose-pine-hyprcursor.url = "github:ndom91/rose-pine-hyprcursor";
  };

  outputs = { nixpkgs, home-manager, ... } @ inputs:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};

      # Modules I use for my personal computers.
      personalModules = [
        ./pure_home.nix
        ./core.nix
        ./zsh.nix
        ./helix.nix
        ./disable_baloo.nix
        ./theming.nix

        ./gitconfig.nix
        ./iamb.nix
        # ./email.nix
        ./calendar.nix
        # ./bch_outlook.nix

        ./packages/basic.nix
        ./packages/programming.nix
        ./packages/personal.nix
        ./packages/jetbrains.nix
        ./packages/allow_unfree.nix

        ./hyprland/packages.nix
        ./hyprland/input.nix
        ./hyprland/visual.nix
        ./hyprland/keybinds.nix
        ./hyprland/services.nix
        ./hyprland/waybar.nix
        ./hyprland/anyrun.nix
        ./hyprland/foot_terminal.nix
        ./hyprland/hyprlock.nix
      ];
    in {
      # My desktop PC.
      homeConfigurations."jenni@geo" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = personalModules ++ [
          ./hyprland/nvidia_and_notnixos_workarounds.nix
          ./my_desktop.nix
        ];
        extraSpecialArgs = { inherit inputs; };
      };

      # My laptop PC.
      homeConfigurations."jenni@anemo" = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = personalModules ++ [
          ./my_laptop.nix
        ];
        extraSpecialArgs = { inherit inputs; };
      };

      # Config I use at work. Use the wrapper script defined in `fnndsc_bch.nix` called "fnndsc-home-manager" to build this.
      homeConfigurations.restricted = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        modules = [
          ./impure_home.nix  # username is my employee ID, I don't want to store that in git.
          ./fnndsc_bch.nix
          ./core.nix
          ./zsh.nix
          ./helix.nix
          ./disable_baloo.nix

          ./gitconfig.nix
          ./iamb.nix
          # ./email.nix
          # ./calendar.nix
          # ./bch_outlook.nix

          ./packages/basic.nix
          ./packages/programming.nix
        ];
        extraSpecialArgs = { inherit inputs; };
      };
    };
}
