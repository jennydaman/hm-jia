{ lib, pkgs, inputs, ... }:

{
  programs.eza.enable = true;
  programs.lsd = {
    enable = true;
    enableAliases = true;
  };
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
  };
  programs.thefuck = {
    enable = true;
    enableZshIntegration = true;
  };
  programs.zsh = {
    enable = true;
    syntaxHighlighting.enable = true;
    autosuggestion.enable = true;
    enableCompletion = true;
    historySubstringSearch.enable = true;
    history.expireDuplicatesFirst = true;
    history.ignoreSpace = true;
    history.ignoreAllDups= true;

    envExtra = ''
      # https://github.com/Qix-/better-exceptions
      export BETTER_EXCEPTIONS=1
    '';

    initExtra = ''
      # completion waiting dots
      # https://github.com/ohmyzsh/ohmyzsh/blob/cf347ef3e43eabde5ae86e17d25690ebbeef5e6b/lib/completion.zsh#L61
      expand-or-complete-with-dots() {
        print -Pn "%F{red}...%f"
        zle expand-or-complete
        zle redisplay
      }
      zle -N expand-or-complete-with-dots
      # Set the function as the default tab completion widget
      bindkey -M emacs "^I" expand-or-complete-with-dots
      bindkey -M viins "^I" expand-or-complete-with-dots
      bindkey -M vicmd "^I" expand-or-complete-with-dots

      # show options when pressing tab, select first option
      # after pressing tab a second time
      setopt nomenucomplete

      # up/down keys search history
      autoload -U up-line-or-beginning-search
      autoload -U down-line-or-beginning-search
      zle -N up-line-or-beginning-search
      zle -N down-line-or-beginning-search
      bindkey "^[[A" up-line-or-beginning-search # Up
      bindkey "^[[B" down-line-or-beginning-search # Down

      # search history with CTRL-R
      bindkey -v
      bindkey '^R' history-incremental-search-backward

      # fix Home, End, and Delete keys
      # https://stackoverflow.com/a/33049500
      bindkey  "^[[H"   beginning-of-line
      bindkey  "^[[F"   end-of-line
      bindkey  "^[[3~"  delete-char

      if [ -f "$HOME/opt/CIVET/quarantines/Linux-x86_64/init.sh" ]; then
        alias n='source "$HOME/opt/CIVET/quarantines/Linux-x86_64/init.sh" && export PATH="$HOME/opt/bin:$PATH"'
      fi
      if [ -e "$HOME/opt/freesurfer" ]; then
        export FREESURFER_HOME="$HOME/opt/freesurfer"
        alias nn='source "$FREESURFER_HOME/FreeSurferEnv.sh"'
      fi

      if [ -e "$HOME/micromamba" ] && type micromamba > /dev/null 2>&1; then
        export MAMBA_ROOT_PREFIX="$HOME/micromamba"
        eval "$(micromamba shell hook --shell zsh --root-prefix "$MAMBA_ROOT_PREFIX")"
      fi
    '';
  };
}
