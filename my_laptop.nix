{ config, lib, pkgs, ... }:

{
  wayland.windowManager.hyprland.settings.monitor = [
    # Dell Latitude 7490 laptop display
    "eDP-1, preferred, 0x0, 1"
  ];

  programs.waybar.settings.mainBar.modules-right = [
    "tray"
    "pulseaudio"
    "network"
    "bluetooth"
    "cpu"
    "memory"
    "temperature"
    "idle_inhibitor"
    "battery"
  ];

  wayland.windowManager.hyprland.settings.plugin.hyprexpo.columns = 2;

  services.hypridle.settings.listener = [
    {
      timeout = 150;
      on-timeout = "${pkgs.brightnessctl}/bin/brightnessctl -s set 10";
      on-resume = "${pkgs.brightnessctl}/bin/brightnessctl -r";
    }
    {
      timeout = 150;
      on-timeout = "${pkgs.brightnessctl}/bin/brightnessctl -sd rgb:kbd_backlight set 0";
      on-resume = "${pkgs.brightnessctl}/bin/brightnessctl -rd rgb:kbd_backlight";
    }
    {
      timeout = 300;
      on-timeout = "loginctl lock-session";      # lock screen when timeout has passed.
    }
    {
      timeout = 330;
      on-timeout = "${pkgs.hyprland}/bin/hyprctl dispatch dpms off";  # screen off when timeout has passed
      on-resume = "${pkgs.hyprland}/bin/hyprctl dispatch dpms on";    # screen on when activity is detected after timeout has fired.
    }
    {
      timeout = 1800;
      on-timeout = "systemctl suspend";
    }
  ];

  home.packages = with pkgs; [
    bluetuith  # bluetooth TUI
    gnome-power-manager
  ];
}
