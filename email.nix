
{ lib, pkgs, inputs, ... }:

{

  home.packages = with pkgs; [
    inputs.dovemail.packages.${system}.default
  ];

  accounts.email.accounts."Jennings.Zhang@childrens.harvard.edu" = {
    primary = true;
    address = "Jennings.Zhang@childrens.harvard.edu";
    aliases = [ "dev@babymri.org" "Newborn_FNNDSCdev-dl@childrens.harvard.edu" ];
    realName = "Jennings Zhang";
    aerc.enable = true;
    aerc.extraAccounts = {
      source = "maildir://~/.local/share/mail/Jennings.Zhang@childrens.harvard.edu";
      check-mail = "10m";
      check-mail-cmd = "~/.dotfiles/bin/pull_emails_using_dovemail.sh";
      check-mail-timeout = "9m";
    };
  };

  programs.aerc.enable = true;
  programs.aerc.extraConfig = {
    # suppress warning about file permissions.
    # ref: https://github.com/nix-community/home-manager/blob/9036fe9ef8e15a819fa76f47a8b1f287903fb848/modules/programs/aerc.nix#L172-L181
    general.unsafe-accounts-conf = true;

    ui.sort = "-r date";
    filters = {
      "text/plain" = "colorize";
      "text/html" = "html | colorize";
      "text/calendar" = "calendar";
      ".headers" = "colorize";
    };
  };
}
