{ config, lib, pkgs, inputs, ... }:

{
  wayland.windowManager.hyprland.settings.monitor = [];

  programs.waybar.settings.mainBar.modules-right = [
    "tray"
    "pulseaudio"
    "network"
    "cpu"
    "memory"
    "temperature"
    "idle_inhibitor"
  ];

  wayland.windowManager.hyprland.settings.plugin.hyprexpo.columns = 3;

  local.jetbrainsMemoryLimit = "16G";

  programs.git = {
    signing.signByDefault = true;
    signing.key = "~/.ssh/id_ed25519.pub";
    extraConfig.gpg.format = "ssh";
  };

  # https://nix-community.github.io/home-manager/index.xhtml#sec-usage-gpu-non-nixos
  nixGL = {
    packages = inputs.nixgl.packages;
    defaultWrapper = "nvidia";
  };
}
