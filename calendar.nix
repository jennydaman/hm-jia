{ lib, pkgs, inputs, ... }:

{
  accounts.calendar.accounts = let
    nextcloudCalendarRemote = key: {
      url = "https://cloud.jennin.xyz/remote.php/dav/calendars/jenni/${key}/";
      type = "caldav";
      userName = "jenni";
      passwordCommand = [ "keyring" "get" "xyz.jennin.cloud@home-manager" "jenni" ];
    };
  in {
    personal = {
      primary = true;
      name = "Personal";
      remote = (nextcloudCalendarRemote "personal-1");
      qcal.enable = true;
    };
    hobbies = {
      name = "Hobbies";
      remote = (nextcloudCalendarRemote "hobbies");
      qcal.enable = true;
    };
    work = {
      name = "Work";
      remote = (nextcloudCalendarRemote "work");
      qcal.enable = true;
    };
    events = {
      name = "Events";
      remote = (nextcloudCalendarRemote "events");
      qcal.enable = true;
    };
  };
  programs.qcal.enable = true;
}
