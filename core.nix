{ config, lib, pkgs, ... }:

{
  nix = {
    package = pkgs.nix;
    settings.experimental-features = [ "nix-command" "flakes" ];
  };

  # This value determines the Home Manager release that your configuration is
  # compatible with. This helps avoid breakage when a new Home Manager release
  # introduces backwards incompatible changes.
  #
  # You should not change this value, even if you update Home Manager. If you do
  # want to update the value, then make sure to first check the Home Manager
  # release notes.
  home.stateVersion = "23.11"; # Please read the comment before changing.

  # Home Manager can also manage your environment variables through
  # 'home.sessionVariables'. If you don't want to manage your shell through Home
  # Manager then you have to manually source 'hm-session-vars.sh' located at
  # either
  #
  #  ~/.nix-profile/etc/profile.d/hm-session-vars.sh
  #
  # or
  #
  #  /etc/profiles/per-user/jenni/etc/profile.d/hm-session-vars.sh
  #
  home.sessionVariables = {
    PNPM_HOME = "$HOME/.local/pnpm-home";
    PATH = "$HOME/bin:$HOME/.cargo/bin:$HOME/.local/bin:$HOME/.bun/bin:$PNPM_HOME:$PATH";
    LC_TIME = "en_DK.UTF-8";  # ISO-8601 time stamps
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
