{ config, lib, pkgs, ... }:

{
  home.username = if builtins.getEnv "USER" == "" then abort "Jennings' Home Manager configuration must be built with --impure" else builtins.getEnv "USER";
  home.homeDirectory = builtins.getEnv "HOME";
}
