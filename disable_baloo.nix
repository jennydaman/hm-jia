{ config, lib, pkgs, ... }:

{
  home.file.".config/baloofilerc".text = ''
    [Basic Settings]
    Indexing-Enabled=false
  '';
}
