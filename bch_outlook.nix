{ lib, pkgs, ... }:
{
  accounts.calendar.accounts.bch_outlook = {
    name = "BCH Outlook";
    remote = {
      url = builtins.readFile ./secret/bch_outlook.txt;
      type = "caldav";
    };
    qcal.enable = true;
  };
}
