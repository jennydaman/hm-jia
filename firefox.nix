# unused:
# - firefox installed from nixpkgs tends not to work as well as installed from ArchLinux's repo
# - Configuration of firefox using home-manager has few options, is tedious, and does not play nicely with Firefox sync
{ config, lib, pkgs, ... }:

{
  programs.firefox.enable = true;
  programs.firefox.profiles."hm-jenni" = {
    id = 0;
    isDefault = true;
    name = "hm-jenni";
    search = {
      force = true;
      default = "StartPage";
      order = [ "StartPage" "Wikipedia" "GitHub" "Nix Packages" "home-manager" ];
      engines = {
        # https://www.startpage.com/sp/cdn/opensearch/en/opensearch.xml
        "StartPage" = {
          urls = [{
            template = "https://www.startpage.com/sp/search?query={searchTerms}";
          }];
          iconUpdateURL = "https://www.startpage.com/favicon.ico";
          updateInterval = 24 * 60 * 60 * 1000; # every day
          definedAliases = [ "@sp" ];
        };
        # https://wiki.archlinux.org/opensearch_desc.php
        "ArchWiki" = {
          urls = [{
            template = "https://wiki.archlinux.org/index.php?search={searchTerms}title=Special:Search";
          }];
          iconUpdateURL = "https://wiki.archlinux.org/favicon.ico";
          updateInterval = 24 * 60 * 60 * 1000; # every day
        };
        "Nix Packages" = {
          urls = [{
            template = "https://search.nixos.org/packages";
            params = [
              { name = "type"; value = "packages"; }
              { name = "query"; value = "{searchTerms}"; }
            ];
          }];
          icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
          definedAliases = [ "@np" ];
        };
        "home-manager" = {
          urls = [{
            template = "https://home-manager-options.extranix.com/";
            params = [
              { name = "release"; value = "master"; }
              { name = "query"; value = "{searchTerms}"; }
            ];
          }];
          iconUpdateURL = "https://home-manager-options.extranix.com/images/favicon.png";
          definedAliases = [ "@hm" ];
        };
        "Bing".metaData.hidden = true;
        "Google".metaData.hidden = true;
        "Amazon.com".metaData.hidden = true;
        "eBay".metaData.hidden = true;
      };
    };
    containers = {
      # color is one of:
      # "blue", "turquoise", "green", "yellow", "orange", "red", "pink", "purple", "toolbar"
      #
      # icon is one of:
      # "briefcase", "cart", "circle", "dollar", "fence", "fingerprint", "gift", "vacation", "food", "fruit", "pet", "tree", "chill"
      home = {
        id = 0;
        name = "Home";
        icon = "fingerprint";
        color = "blue";
      };
      libre = {
        id = 1;
        name = "Libre";
        icon = "gift";
        color = "turquoise";
      };
      NEU = {
        id = 2;
        name = "NEU";
        icon = "pet";
        color = "red";
      };
      BCH = {
        id = 3;
        name = "BCH";
        icon = "briefcase";
        color = "pink";
      };
      proton = {
        id = 4;
        name = "Proton";
        color = "purple";
        icon = "fingerprint";
      };
    };
    containersForce = true;
  };
}
