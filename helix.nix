{ lib, pkgs, inputs, ... }:
{
  programs.helix = {
    enable = true;
    defaultEditor = true;
    settings = {
      theme = "jellybeans_transparent";
      editor.true-color = true;
      editor.soft-wrap.enable = true;
    };
  };

  home.file.".config/helix/themes/jellybeans_transparent.toml".source = (pkgs.formats.toml { }).generate "helix_themes_jellybeans_transparent" {
    inherits = "jellybeans";
    "ui.background" = "";  # transparent background
  };

  home.sessionVariables = {
    EDITOR = "hx";
    VISUAL = "hx";
  };
}
