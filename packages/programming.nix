{ lib, pkgs, inputs, ... }:

{
  programs.bun.enable = true;

  home.packages = with pkgs; let
    # see https://wiki.nixos.org/wiki/Helm_and_Helmfile#HELMFILE
    my-kubernetes-helm = with pkgs; wrapHelm kubernetes-helm {
      plugins = with pkgs.kubernetes-helmPlugins; [
        helm-secrets
        helm-diff
        helm-s3
        helm-git
        helm-unittest
      ];
    };
    my-helmfile = pkgs.helmfile-wrapped.override {
      inherit (my-kubernetes-helm) pluginsDir;
    };
  in [
    # Programming Languages
    # ---------------------
    
    corepack  # yarn, pnpm
    nodejs
    
    rustup
    cargo-llvm-cov
    cargo-nextest
    cargo-watch
    evcxr

    python312
    python312Packages.ipython
    uv
    rye

    (pkgs.buildFHSEnv {
      name = "pixi";
      runScript = "pixi";
      targetPkgs = pkgs: with pkgs; [ pixi ];
    })
    (pkgs.buildFHSEnv {
      name = "micromamba";
      runScript = "micromamba";
      targetPkgs = pkgs: with pkgs; [ micromamba ];
    })

    nix-init

    # For work
    # --------
    inputs.chrs.packages.${system}.default
    kind
    k9s
    kubectl
    openshift
    my-kubernetes-helm
    my-helmfile
    kustomize
    helm-docs

    datalad
    git-annex
  ];

  programs.kubecolor = {
    enable = true;
    enableAlias = true;
  };

  programs.helix = {
    extraPackages = with pkgs; [
      marksman
      nodePackages.typescript-language-server
      svelte-language-server
      helm-ls
    ];
    settings.editor.lsp = {
      enable = true;
      display-messages = true;
      display-inlay-hints = true;
    };
    languages = {
      language = [
        {
          # NOTE: switch to pylyzer once it is feature complete.
          # https://github.com/mtshiba/pylyzer
          name = "python";
          language-servers = [ "pyright" "ruff" ];
        }
      ];
      language-server = {
        ruff = {
          command = "${pkgs.ruff}/bin/ruff";
          args = [ "server" ];
        };
        pyright = {
          command = "${pkgs.pyright}/bin/pyright-langserver";
          args = [ "--stdio" ];
          config.python.analysis = {
            # one of: "off", "basic", "standard", "strict"
            # see https://github.com/microsoft/pyright/blob/main/docs/configuration.md#diagnostic-settings-defaults
            typeCheckingMode = "strict";
          };
        };
      };
    };
  };
}
