{ lib, pkgs, inputs, ... }:

{
  programs.fastfetch.enable = true;
  programs.yt-dlp.enable = true;
  programs.bottom.enable = true;

  programs.jq.enable = true;
  programs.fd.enable = true;
  programs.bat.enable = true;
  programs.ripgrep.enable = true;
  programs.fzf.enable = true;

  programs.zellij = {
    enable = true;
    enableZshIntegration = false;
  };

  # The home.packages option allows you to install Nix packages into your
  # environment.
  home.packages = with pkgs; [

    # # It is sometimes useful to fine-tune packages, for example, by applying
    # # overrides. You can do that directly here, just don't forget the
    # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
    # # fonts?
    # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })

    # # You can also create simple shell scripts directly inside your
    # # configuration. For example, this adds a command 'my-hello' to your
    # # environment:
    # (pkgs.writeShellScriptBin "my-hello" ''
    #   echo "Hello, ${config.home.username}!"
    # '')

    # monitoring tools
    htop
    btop
    bandwhich
    gping
    traceroute

    # command-line tools
    xh
    tealdeer
    trash-cli
    glow
    erdtree
    dua
    rclone
    httplz
    file
    wget
    pfetch-rs
    parallel
    yq-go
    jnv
    tabiew
    sad
    units
    toilet
    graphicsmagick
    graphicsmagick-imagemagick-compat
    poppler_utils  # provides pdfunite
    xcp
    dogdns

    just
    typst
    ffmpeg

    # git-related and code tools
    git
    delta
    difftastic
    scc

    # fonts
    font-awesome

    # fun
    asciiquarium
    doge
    dotacat

    (writeShellScriptBin "ssht" ''
      # a helper script for doing a bunch of SSH port forwards.
      ports=(
        3000 4005 4222
        {5173..5179}
        {7860..7869}
        {8000..8040} 8042 8080 8088 8090 8888
        25173
        {32000..32010} 32020
      )
      cmd=('${pkgs.openssh_hpn}/bin/ssh')
      for port in "''${ports[@]}"; do
        cmd+=('-L' "localhost:$port:localhost:$port")
      done
      cmd+=("$@")
      set -ex
      exec "''${cmd[@]}"
    '')
  ];
}
