# Stuff I want installed on my personal devices, but not my work computer.
# Mostly GUI applications.

{ lib, pkgs, inputs, config, ... }:

{
  home.packages = with pkgs; let
    # helper function to modify .desktop files
    # https://redlib.jennin.xyz/r/NixOS/comments/scf0ui/how_would_i_update_desktop_file/hu9xwv2/?context=3
    patchDesktop = pkg: appName: from: to: lib.hiPrio (pkgs.runCommand "$patched-desktop-entry-for-${appName}" {} ''
      ${coreutils}/bin/mkdir -p $out/share/applications
      ${gnused}/bin/sed 's#${from}#${to}#g' < ${pkg}/share/applications/${appName}.desktop > $out/share/applications/${appName}.desktop
    '');
  in [
    # patched to force GTK dark theme
    virt-manager
    (patchDesktop virt-manager "virt-manager" "^Exec=.*virt-manager.*$" "Exec=env GTK_THEME=Adwaita:dark virt-manager %U")

    ungoogled-chromium
    signal-desktop
    anytype
    siyuan

    parsec-bin
    vdhcoapp    # Video Download Helper
    pulsemixer  # audio mixer TUI
    immich-go
    sniffnet

    # Security
    gnupg
    apg
    encfs
    sshfs
    sshpass
    python312Packages.keyring

    # multi-media
    mpv
    xplorer  # file manager
    picard

    # academic-ish
    zotero
    sioyek

    # graphics editors
    inkscape
    gimp
    scribus  # recommended to me by Vincent

    # GUI IDEs and development
    lapce
    vscodium
    # rstudio

    # VNC client
    # tigervnc has some bugs with TLS, but turbovnc seems to work
    # tigervnc
    # turbovnc

    # GNOME applications
    nautilus
    eog
    seahorse
    evince
    gradience  # theming

    # NOTE: the packages below do not work well.
    # Suggestion: install them via Flatpak or other package manager.
    # libreoffice-fresh
    # hunspell
    # hunspellDicts.en_US
    # firefox
    # vlc

    # KDE applications
    # I'm having problems with KDE applications installed from Nix repos
    # used outside of KDE.
    # https://discourse.nixos.org/t/my-progress-with-nixos-hyprland/37585/2?u=jennydaman
    # However, they seem to work well from Flatpak.

    # I like dde-file-manager, but its dark theme settings do not get saved
    # which might be because I am not running the Deepin settings daemon
    # deepin.dde-file-manager
  ];
}
