# - Installs Jetbrains IDEs.
# - Increase JetBrains IDE memory limit.
# - When running on Hyprland, use Wayland.

{ config, lib, pkgs, ... }:
{
  options = {
    local.jetbrainsMemoryLimit = lib.mkOption {
      type = lib.types.str;
      default = "4G";
      example = "8G";
      description = ''
        Maximum memory heap size that the JVM can allocate for running Jetbrains IDEs.
        See https://www.jetbrains.com/help/idea/tuning-the-ide.html#xmx
      '';
    };
  };

  config = let
    memoryFlag = "-Xmx${config.local.jetbrainsMemoryLimit}";
  in {

    # packages to install
    home.packages = with pkgs.jetbrains; [rust-rover pycharm-professional webstorm];
  
    # set IDEA_VM_OPTIONS default value
    home.sessionVariables = let
      configFile = pkgs.writeText "jetbrains.vmoptions" "${memoryFlag}";
    in {
      "IDEA_VM_OPTIONS" = configFile;
    };
    # override IDEA_VM_OPTIONS in Hyprland
    wayland.windowManager.hyprland.settings.env = let
      configFile = pkgs.writeText "jetbrains.vmoptions" ''
        -Dawt.toolkit.name=WLToolkit
        ${memoryFlag}
      ''; in
    [
      "IDEA_VM_OPTIONS,${configFile}"
    ];
  };
}
