{ lib, pkgs, inputs, ... }:

{
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "anytype"
    "anytype-heart"
    "rust-rover"
    "pycharm-professional"
    "webstorm"
    "parsec-bin"
  ];
}
